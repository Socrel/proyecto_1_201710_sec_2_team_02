package p1_201710;

import java.util.Comparator;

import junit.framework.TestCase;
import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.Queue;
import model.logic.CompararAgnoPelicula;
import model.logic.MergeSort;
import model.vo.VOPelicula;

public class testMergeSort extends TestCase
{
	/**
	 * Atributo que modela el Stack para testear
	 */
	private MergeSort<VOPelicula> merge = new MergeSort<VOPelicula>();
	private ListaEncadenada<VOPelicula> lista = new ListaEncadenada<VOPelicula>();
	private ILista<VOPelicula> ordenada = new ListaEncadenada<VOPelicula>();

	public static int A = 43;
	public static int B = 12;
	public static int C = 24;
	public static int D = 72;
	/**
	 * Escenario uno
	 */
	
	public void setupEscenario1()
	{
		lista = new ListaEncadenada<VOPelicula>();
	}

	public void setupEscenario2()
	{
		VOPelicula peli1 = new VOPelicula();
		VOPelicula peli2 = new VOPelicula();
		VOPelicula peli3 = new VOPelicula();
		VOPelicula peli4 = new VOPelicula();
		Comparator<VOPelicula> comp = new CompararAgnoPelicula(); 
		
		peli1.setAgnoPublicacion(A);
		lista.agregarElementoFinal(peli1);

		peli2.setAgnoPublicacion(B);
		lista.agregarElementoFinal(peli2);

		peli3.setAgnoPublicacion(C);
		lista.agregarElementoFinal(peli3);
	
		peli4.setAgnoPublicacion(D);
		lista.agregarElementoFinal(peli4);
	
		merge.ordenarPelicula(comp, lista);
		ordenada = merge.ordenarPelicula(comp, lista);
		System.out.println(ordenada.darElemento(0));
		
	}
	public void testOrdenar(){
		setupEscenario2();
		assertTrue(lista.darNumeroElementos()!= 0);
		assertTrue(ordenada.darElemento(0)!= null);
	}
	
	/**
	 * test isEmpty
	 */
	public void testIsEmpty()
	{
		//Prueba 1, cuando esta vac�a la cola
		setupEscenario1();
		assertTrue(lista.darNumeroElementos() == 0);

		//Prueba 2; cuando no esta vac�a
		setupEscenario2();
		assertFalse(lista.darNumeroElementos() == 0);
	}
}