package model.data_structures;


public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> cabeza;
	private NodoDoble<T> actual;
	private NodoDoble<T> ultimo;
	private NodoDoble<T> mitad;
	private int tamLista;

	public ListaDobleEncadenada()
	{
		cabeza= null;
		actual=cabeza;
		ultimo = null;
		mitad = null;
		tamLista = 0;
		
	}
	

	public void eliminar(){
		if(cabeza.darSiguiente()!=null)
		{
		cabeza.darSiguiente().cambiarAnterior(null);
		cabeza.cambiarSiguiente(null);
		cabeza = null;
		actual = null;
		ultimo = null;
		}
		else{
			cabeza=null;
			actual = null;
			ultimo=null;
		}
		tamLista=0;
	}
	public void agregarElementoFinal(T elem) {
		NodoDoble<T> nuevo = new NodoDoble<T>(elem);
		if(cabeza == null){
			cabeza = nuevo;
			actual = cabeza;
			ultimo = cabeza;
		}
		else {
			ultimo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimo);
			ultimo = nuevo;
		}
		tamLista++;	
	}
	
	public void asignarElementoMitad()
	{
		mitad.cambiarActual(darElemento(tamLista/2));
	}
	public T darElementoMitad()
	{
		return mitad.darActual();
	}
	public T darElementoFinal()
	{
		return ultimo.darActual();
	}
	public T darElementoSiguiente()
	{
		if(actual.darSiguiente()!=null)
		{
		actual= actual.darSiguiente();
		return actual.darActual();
		}
		else 
		{
			actual=null;
			return null;
		}
			
	}
	public T darElementoAnterior()
	{
		if(actual.darAnterior()!=null)
		{
		actual= actual.darAnterior();
		return actual.darActual();
		}
		else 
		{
			actual=null;
			return null;
		}
	}
	public void reiniciarActual()
	{
		actual=cabeza;
	}
	public T darElemento(int pos) {

		T resp=null;
		int contador=0;
		NodoDoble<T> actual= cabeza;
		while(actual!=null)
		{
			T ob= actual.darActual();
			if (contador==pos) {
				resp=ob;
				break;
			}
			actual=actual.darSiguiente();
			contador++;
		}
		return resp;
	}


	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return tamLista;
	}

	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual.darActual();
	}

	public NodoDoble<T> darUltimo(){
		return ultimo;
	}
	public void ordenar() {
		
	}


	public T eliminarElemento() {
		// TODO Auto-generated method stub
		return null;
	}
}
