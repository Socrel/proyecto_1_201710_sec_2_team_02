package p1_201710;

import model.data_structures.Queue;
import junit.framework.TestCase;

public class testQueue extends TestCase
{
	/**
	 * Atributo que modela el Stack para testear
	 */
	private Queue<String> colaTest;

	public static String A = "Saw";
	public static String B = "Piratas";
	public static String C = "Divergente";
	public static String D = "Lego";
	/**
	 * Escenario uno
	 */
	
	public void setupEscenario1()
	{
		colaTest = new Queue<>();
	}

	public void setupEscenario2()
	{
		colaTest = new Queue<>();
		colaTest.enqueue(A);
		colaTest.enqueue(B);
		colaTest.enqueue(C);
		colaTest.enqueue(D);
	}
	/**
	 * Test agregar item, enqueue
	 */
	public void testEnqueue()
	{
		setupEscenario2();
		assertEquals("No fue lo esperado.", A,colaTest.dequeue());
		assertEquals("No fue lo esperado.", B,colaTest.dequeue());
		assertEquals("No fue lo esperado.", C,colaTest.dequeue());
		assertEquals("No fue lo esperado.", D,colaTest.dequeue());
	}

	/**
	 * Test eliminar primer elemento de la lista, dequeue
	 */
	public void tesDequeue()
	{
		setupEscenario2();
		assertEquals("No fue lo esperado.", A,colaTest.dequeue());
		assertEquals("No fue lo esperado.", B,colaTest.dequeue());
		assertEquals("No fue lo esperado.", C,colaTest.dequeue());
		assertEquals("No fue lo esperado.", D,colaTest.dequeue());
	}

	/**
	 * test isEmpty
	 */
	public void testIsEmpty()
	{
		//Prueba 1, cuando esta vac�a la cola
		setupEscenario2();
		assertTrue(colaTest.isEmpty());

		//Prueba 2; cuando no esta vac�a
		setupEscenario1();
		assertFalse(colaTest.isEmpty());
	}
	
	/**
	 * test size
	 */
	public void testSize()
	{
		setupEscenario2();
		assertNotNull( "Deber�a retornar una pila.", colaTest);
		assertEquals("La lista deber�a tener 4.", 4, colaTest.size());
	}
}