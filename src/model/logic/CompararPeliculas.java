package model.logic;

import java.util.Comparator;

import model.vo.VOPelicula;

public class CompararPeliculas implements Comparator{

	public int compare(Object peli1, Object peli2) {
		
		if(((VOPelicula) peli1).getAgnoPublicacion()>(((VOPelicula) peli2).getAgnoPublicacion()))
			return 1;
		else if (((VOPelicula) peli1).getAgnoPublicacion()<(((VOPelicula) peli2).getAgnoPublicacion()))
			return -1;
		else
		{
			if(((VOPelicula) peli1).getPromedioRatings()>(((VOPelicula) peli2).getPromedioRatings()))
				return 1;
			else if(((VOPelicula) peli1).getPromedioRatings()<(((VOPelicula) peli2).getPromedioRatings()))
				return -1;
			else
			{
				return ((VOPelicula) peli1).getTitulo().compareToIgnoreCase(((VOPelicula) peli2).getTitulo());
			}
		}
	}

}
