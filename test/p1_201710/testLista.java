package p1_201710;

import model.data_structures.ListaEncadenada;
import junit.framework.TestCase;

public class testLista extends TestCase {

	private ListaEncadenada<String> lista;
	public static String A = "Saw";
	public static String B = "Piratas";
	public static String C = "Divergente";
	public static String D = "Lego";

	/**
	 * Escenario Lista Vacia
	 */
	public void setupEscenario1(){
		
		lista = new ListaEncadenada<>();
	
	}
	
	/**
	 * Escenario Lista con datos
	 */
	public void setupEscenario2(){
		
		lista = new ListaEncadenada<String>();
		
		lista.agregarElementoFinal(A);
		lista.agregarElementoFinal(B);
		lista.agregarElementoFinal(C);
		lista.agregarElementoFinal(D);
	}

	/**
	 * Test lista vacia
	 * Probamos el n�mero de elementos.
	 * Probamos si el primer elemento es null. 
	 */
	public void testListaVacia(){
		
		setupEscenario1();
	
		assertEquals("La lista deber�a estar vac�a.", 0 ,lista.darNumeroElementos());
		assertEquals("La lista deber�a estar vac�a.", null ,lista.darElemento(0));
		
	}
	/**
	 * Probamos si se agregan los elementos a la lista. 
	 */
	public void testAgregarElementoFinal(){
		
		setupEscenario2();
		
		assertEquals("No fue lo esperado.", A,lista.darElemento(0));
		assertEquals("No fue lo esperado.", B,lista.darElemento(1));
		assertEquals("No fue lo esperado.", C,lista.darElemento(2));
		assertEquals("No fue lo esperado.", D,lista.darElemento(3));
	}

	/**
	 * Probamos el metodo darNumeroElementos desp�es de agregar los datos.
	 */
	public void testNumeroDeElementos(){
		
		setupEscenario2();
		
		assertNotNull( "Deber�a retornar una lista.", lista);
		assertEquals("La lista deber�a tener 4.", 4, lista.darNumeroElementos());
	}

}
