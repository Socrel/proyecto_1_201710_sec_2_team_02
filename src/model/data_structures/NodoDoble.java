package model.data_structures;

import java.util.Iterator;

public class NodoDoble <T>{
	
	private NodoDoble siguiente;
	private NodoDoble anterior;
	private T elem;
	
		public NodoDoble (T elem){
			this.elem = elem;
			siguiente = null;
			anterior = null;
		}
		
	public NodoDoble<T> darSiguiente(){
		if (siguiente!=null)
			return siguiente;
		return null;
	}
	public NodoDoble<T> darAnterior(){
		return anterior;
	}
	public T darActual(){
		return elem;
	}
	public void cambiarActual(T act)
	{
		elem = act;
	}
	public void cambiarSiguiente(NodoDoble<T> sig){
		siguiente = sig;
	}
	public void cambiarAnterior(NodoDoble<T> ant){
		anterior = ant;
	}
}
