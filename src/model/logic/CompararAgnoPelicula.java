package model.logic;

import java.util.Comparator;

import model.vo.VOPelicula;

public class CompararAgnoPelicula implements Comparator{

	@Override
	public int compare(Object peli1, Object peli2) {
		if(((VOPelicula) peli1).getAgnoPublicacion()>(((VOPelicula) peli2).getAgnoPublicacion()))
			return 1;
		else if (((VOPelicula) peli1).getAgnoPublicacion()<(((VOPelicula) peli2).getAgnoPublicacion()))
			return -1;
		return 0;
	}

}
