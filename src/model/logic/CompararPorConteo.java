package model.logic;

import java.util.Comparator;

import model.vo.VOUsuarioConteo;

public class CompararPorConteo implements Comparator{


	public int compare(Object usuario1, Object usuario2) {
		
	if(( (VOUsuarioConteo)usuario1).getConteo() > ((VOUsuarioConteo)usuario2).getConteo()){
		
		return 1;
	}
	else if(((VOUsuarioConteo)usuario1).getConteo() < ((VOUsuarioConteo)usuario2).getConteo()){
		return -1;
	}
	else 
		return 0;
	}

}
