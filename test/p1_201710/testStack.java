package p1_201710;

import model.data_structures.Stack;
import junit.framework.TestCase;

public class testStack extends TestCase
{
	/**
	 * Atributo que modela el Stack para testear
	 */
	private Stack<String> pilaTest;

	public static String A = "Saw";
	public static String B = "Piratas";
	public static String C = "Divergente";
	public static String D = "Lego";
	/**
	 * Escenario uno
	 */
	
	public void setupEscenario1()
	{
		pilaTest = new Stack<>();
	}

	public void setupEscenario2()
	{
		pilaTest = new Stack<>();
		pilaTest.push(A);
		pilaTest.push(B);
		pilaTest.push(C);
		pilaTest.push(D);
	}
	/**
	 * Test agregar item, enqueue
	 */
	public void testPush()
	{
		setupEscenario2();
		assertEquals("No fue lo esperado.", D,pilaTest.pop());
		assertEquals("No fue lo esperado.", C,pilaTest.pop());
		assertEquals("No fue lo esperado.", B,pilaTest.pop());
		assertEquals("No fue lo esperado.", A,pilaTest.pop());
	}

	/**
	 * Test eliminar primer elemento de la lista, dequeue
	 */
	public void testPop()
	{
		setupEscenario2();
		assertEquals("No fue lo esperado.", D,pilaTest.pop());
		assertEquals("No fue lo esperado.", C,pilaTest.pop());
		assertEquals("No fue lo esperado.", B,pilaTest.pop());
		assertEquals("No fue lo esperado.", A,pilaTest.pop());
	}

	/**
	 * test isEmpty
	 */
	public void testIsEmpty()
	{
		//Prueba 1, cuando esta vac�a la cola
		setupEscenario2();
		assertTrue(pilaTest.isEmpty());

		//Prueba 2; cuando no esta vac�a
		setupEscenario1();
		assertFalse(pilaTest.isEmpty());
	}
	
	/**
	 * test size
	 */
	public void testSize()
	{
		setupEscenario2();
		assertNotNull( "Deber�a retornar una pila.", pilaTest);
		assertEquals("La lista deber�a tener 4.", 4, pilaTest.size());
	}
}
