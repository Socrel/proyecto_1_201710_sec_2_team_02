package controller;

import api.ISistemaRecomendacionPeliculas;
import api.SistemaRecomendacionPeliculas;

public class Controller {

	private static ISistemaRecomendacionPeliculas manejador= new SistemaRecomendacionPeliculas();
	
	public static void cargarArchivos() {
	
		manejador.cargarPeliculasSR("data/movies.csv");
		manejador.cargarRatingsSR("data/ratings.csv");
		manejador.cargarTagsSR("data/tags.csv");
	}
	
	public static void ordenarUsuarioConteo(int n)
	{
		manejador.usuariosActivosSR(n);
	}
}
