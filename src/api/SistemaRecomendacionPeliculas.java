package api;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoSimple;
import model.data_structures.Queue;
import model.logic.CompararAgnoPelicula;
import model.logic.CompararFechaRatings;
import model.logic.CompararPeliculas;
import model.logic.CompararPorConteo;
import model.logic.MergeSort;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;

import java.util.Comparator;
import java.util.StringTokenizer;

import com.csvreader.CsvReader;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas  {

	private ILista<VOPelicula> misPeliculas;
	private ILista<VORating> ratings;
	private ILista<VOTag> tags;
	private ILista<VOUsuario> miUser;
	private ILista<VOGeneroPelicula> genPelicula = new ListaEncadenada<VOGeneroPelicula>();
	private ILista<VOGeneroUsuario> genUser= new ListaEncadenada<VOGeneroUsuario>();
	private MergeSort<VOPelicula> mergePeliculas= new MergeSort<VOPelicula>();
	private MergeSort<VOUsuarioConteo> mergeConteo= new MergeSort<VOUsuarioConteo>();
	private MergeSort<VOUsuario> mergeUsuarioT= new MergeSort<VOUsuario>();
	private Queue<VOOperacion> operaciones;
	private int agnio=0;
	private ListaEncadenada<String> tagsIdP = new ListaEncadenada<String>();
	private ListaEncadenada<String> tagsIdU = new ListaEncadenada<String>();
	private ILista<VOGeneroTag> genTag;


	public boolean cargarPeliculasSR(String rutaPeliculas) {
		String cadena = null;
		int anio=0;
		String nombreComple = null;
		misPeliculas = new ListaEncadenada<VOPelicula>();


		try {

			CsvReader peliculasImport = new CsvReader(rutaPeliculas);
			peliculasImport.readHeaders();

			while (peliculasImport.readRecord())
			{
				ListaEncadenada<String> generos = new ListaEncadenada<String>();
				String id = peliculasImport.get("movieId");
				String nombreAnio = peliculasImport.get("title");
				String generosV = peliculasImport.get("genres");

				StringTokenizer st = new StringTokenizer(nombreAnio,"()",false);
				nombreComple = st.nextElement().toString();
				while (st.hasMoreTokens()) { 
					cadena = st.nextToken();
					try {
						anio = Integer.parseInt(cadena);

					} catch (NumberFormatException nfe){
						if(!nombreComple.equals(cadena))
						{
							nombreComple = nombreComple.concat("("+cadena+")");
						}

					}

				}

				String[] generosSpl = generosV.split("\\|");


				for (int i=0; i<generosSpl.length;i++) 
				{

					generos.agregarElementoFinal(generosSpl[i]);
				}

				VOPelicula pelicula = new VOPelicula();

				long idP = Long.parseLong(id);
				pelicula.setIdPelicula(idP);
				pelicula.setTitulo(nombreComple); 	      
				pelicula.setAgnoPublicacion(anio);
				pelicula.setGenerosAsociados(generos);

				for (int i=0; i<generosSpl.length;i++) 
				{
					VOGeneroPelicula generoPelicula= new VOGeneroPelicula();
					ILista<VOPelicula> peliculaDeGenero = new ListaEncadenada<VOPelicula>();
					if(genPelicula.darNumeroElementos()==0)
					{

						generoPelicula.setGenero(generosSpl[i]);
						peliculaDeGenero.agregarElementoFinal(pelicula);
						generoPelicula.setPeliculas(peliculaDeGenero);
						genPelicula.agregarElementoFinal(generoPelicula);
					}
					else{
						boolean encontro = false;
						for(int j=0;j<genPelicula.darNumeroElementos()&& !encontro;j++)
						{
							if(generosSpl[i].equals(genPelicula.darElementoPosicionActual().getGenero()))
							{
								peliculaDeGenero = genPelicula.darElementoPosicionActual().getPeliculas();
								peliculaDeGenero.agregarElementoFinal(pelicula);
								genPelicula.darElementoPosicionActual().setPeliculas(peliculaDeGenero);
								encontro = true;
							}
							else
								genPelicula.darElementoSiguiente();
						}
						genPelicula.reiniciarActual();
						if (encontro != true)
						{	
							generoPelicula.setGenero(generosSpl[i]);
							peliculaDeGenero.agregarElementoFinal(pelicula);
							generoPelicula.setPeliculas(peliculaDeGenero);
							genPelicula.agregarElementoFinal(generoPelicula);
						}
					}

				}



				misPeliculas.agregarElementoFinal(pelicula);
			}
			peliculasImport.close();
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}



	public boolean cargarRatingsSR(String rutaRatings) {
		ratings = new ListaEncadenada<VORating>();
		ListaEncadenada<String> times = new ListaEncadenada<String>();
		miUser = new ListaEncadenada<VOUsuario>();
		try {
			CsvReader ratingsImport = new CsvReader(rutaRatings);
			ratingsImport.readHeaders();
			while (ratingsImport.readRecord())
			{
				String idUser = ratingsImport.get("userId");
				String idMovie = ratingsImport.get("movieId");
				String rating = ratingsImport.get("rating");
				String time = ratingsImport.get("timestamp");


				int idU=Integer.parseInt(idUser);
				int idP=Integer.parseInt(idMovie);
				double rat=Double.parseDouble(rating);

				agregarRating(idU,idP,rat);
				times.agregarElementoFinal(time);
			}
			/**
	        while(misPeliculas.darElementoPosicionActual()!=null)
	        {
	        int cont=0;
	        for(long i=0; i<ratings.darNumeroElementos();i++)
	        {

	        	if(ratings.darElementoPosicionActual().getIdPelicula()==misPeliculas.darElementoPosicionActual().getIdPelicula())
	        	{
	        		cont++;
	        	}
	        	ratings.darElementoSiguiente();
	        }
	        misPeliculas.darElementoPosicionActual().setNumeroRatings(cont);
	        misPeliculas.darElementoSiguiente();
	        }
			 */
			long y=1;
			int contTot=0;
			for(long j=0;j<ratings.darElementoFinal().getIdUsuario();j++)
			{
				int cont =0;
				ListaEncadenada<String> primTime=new ListaEncadenada<String>();
				while (ratings.darElementoPosicionActual().getIdUsuario()==y)
				{
					cont ++;
					primTime.agregarElementoFinal(times.darElementoPosicionActual());

					if(ratings.darElementoSiguiente()==null)
						break;
					times.darElementoSiguiente();
				}
				contTot+=cont;
				VOUsuario userC = new VOUsuario();
				userC.setIdUsuario(y);
				userC.setNumRatings(cont);
				userC.setPrimerTimestamp(Long.parseLong(times.darElemento(contTot-cont)));
				miUser.agregarElementoFinal(userC);
				y++;
			}	
			ratings.reiniciarActual();
			misPeliculas.reiniciarActual();
			times.reiniciarActual();

			for(int h=0; h<ratings.darNumeroElementos();h++)
			{
				long idPelicula =ratings.darElementoPosicionActual().getIdPelicula();
				VOPelicula peliculaB = new VOPelicula();
				VOUsuarioConteo usuarioAgr= new VOUsuarioConteo();
				while(misPeliculas.darElementoPosicionActual()!=null)
				{
					if(misPeliculas.darElementoPosicionActual().getIdPelicula()==idPelicula)
					{
						peliculaB=misPeliculas.darElementoPosicionActual();
						break;
					}
					misPeliculas.darElementoSiguiente();
				}
				misPeliculas.reiniciarActual();
				for (int i=0; i<peliculaB.getGenerosAsociados().darNumeroElementos();i++) 
				{
					VOGeneroUsuario generoUsuario= new VOGeneroUsuario();
					ILista<VOUsuarioConteo> usuarioDeGenero = new ListaEncadenada<VOUsuarioConteo>();

					if(genUser.darNumeroElementos()==0)
					{
						usuarioAgr.setIdUsuario(ratings.darElementoPosicionActual().getIdUsuario());
						usuarioDeGenero.agregarElementoFinal(usuarioAgr);
						generoUsuario.setGenero(peliculaB.getGenerosAsociados().darElementoPosicionActual());
						generoUsuario.setUsuarios(usuarioDeGenero);
						genUser.agregarElementoFinal(generoUsuario);
					}
					else{
						boolean encontro = false;

						for(int j=0;j<genUser.darNumeroElementos()&& !encontro;j++)
						{
							if(peliculaB.getGenerosAsociados().darElementoPosicionActual().equals(genUser.darElementoPosicionActual().getGenero()))
							{
								usuarioAgr.setIdUsuario(ratings.darElementoPosicionActual().getIdUsuario());
								usuarioDeGenero= genUser.darElementoPosicionActual().getUsuarios();
								usuarioDeGenero.agregarElementoFinal(usuarioAgr);
								genUser.darElementoPosicionActual().setUsuarios(usuarioDeGenero);
								encontro = true;
							}
							else
								genUser.darElementoSiguiente();
						}
						genUser.reiniciarActual();
						if (encontro != true)
						{	
							usuarioAgr.setIdUsuario(ratings.darElementoPosicionActual().getIdUsuario());
							usuarioDeGenero.agregarElementoFinal(usuarioAgr);
							generoUsuario.setGenero(peliculaB.getGenerosAsociados().darElementoPosicionActual());
							generoUsuario.setUsuarios(usuarioDeGenero);
							genUser.agregarElementoFinal(generoUsuario);
						}
					}
					peliculaB.getGenerosAsociados().darElementoSiguiente();
				}
				peliculaB.getGenerosAsociados().reiniciarActual();
				ratings.darElementoSiguiente();
			}
			ratings.reiniciarActual();

			ratingsImport.close();
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}



	public boolean cargarTagsSR(String rutaTags) {
		tags = new ListaEncadenada<VOTag>();
		
		try {
			CsvReader tagsImport = new CsvReader(rutaTags);
			tagsImport.readHeaders();

			while (tagsImport.readRecord())
			{
				String idUser = tagsImport.get("userId");
				String idMovie = tagsImport.get("movieId");
				String tag = tagsImport.get("tag");
				String time = tagsImport.get("timestamp");


				VOTag tagC = new VOTag();
				long timetamp=Long.parseLong(time);
				tagC.setTag(tag);
				tagC.setTimestamp(timetamp);

				tags.agregarElementoFinal(tagC);

				tagsIdP.agregarElementoFinal(idMovie);
				tagsIdU.agregarElementoFinal(idUser);

			}

			long x=1;
			while(x<=misPeliculas.darNumeroElementos())
	        {
	        int cont=0;
	        for(long i=0; i<tagsIdP.darNumeroElementos();i++)
	        {

	        	if(Long.parseLong(tagsIdP.darElementoPosicionActual())==misPeliculas.darElementoPosicionActual().getIdPelicula())
	        	{
	        		cont++;
	        	}
	        	tagsIdP.darElementoSiguiente();
	        }
	        tagsIdP.reiniciarActual();
	        misPeliculas.darElementoPosicionActual().setNumeroTags(cont);
	        misPeliculas.darElementoSiguiente();
	        x++;
	        }
	        misPeliculas.reiniciarActual();
	        
			
			tagsImport.close();
			
			misPeliculas.asignarElementoMitad();
			ratings.asignarElementoMitad();
			tags.asignarElementoMitad();
			miUser.asignarElementoMitad();
			genUser.asignarElementoMitad();
			genPelicula.asignarElementoMitad();
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	public int sizeMoviesSR() {
		return misPeliculas.darNumeroElementos();
	}


	public int sizeUsersSR() {
		return Integer.parseInt(miUser.darElementoFinal().getIdUsuario()+"");
	}

	public boolean estaUsuario(ListaEncadenada<VORating> listaP, long object)
	{
		boolean resp=false;
		while(miUser.darElementoPosicionActual()!=null)
		{
			if(object==miUser.darElementoPosicionActual().getIdUsuario())
			{
				resp= true;
				break;
			}
			miUser.darElementoSiguiente();
		}
		miUser.reiniciarActual();
		return resp;
	}

	public int sizeTagsSR() {
		return tags.darNumeroElementos();
	}


	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		ILista<VOGeneroPelicula> listGenPeli = new ListaEncadenada<VOGeneroPelicula>();

		if(genPelicula.darElemento(0)!= null){

			for(int i = 0 ; i < genPelicula.darNumeroElementos(); i++){

				VOGeneroPelicula voGenPel = genPelicula.darElemento(i);
				String genero = voGenPel.getGenero();
				ILista<VOPelicula> pelis = voGenPel.getPeliculas();
				
				VOPelicula mayor = pelis.darElemento(i);
				int ratingMayor = mayor.getNumeroRatings();
				ILista<VOPelicula> peliculaMayorFinal = new ListaEncadenada<VOPelicula>();

				for(int j = 0 ; j < pelis.darNumeroElementos(); j++){

					int busc = pelis.darElemento(j).getNumeroRatings();
					if( ratingMayor < busc){
						ratingMayor = busc;
						mayor = pelis.darElemento(j);
					}
					peliculaMayorFinal.agregarElementoFinal(mayor);
					voGenPel.setPeliculas(peliculaMayorFinal);
					listGenPeli.agregarElementoFinal(voGenPel);
				}
			}
		}
		return null;
	}


	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR(){
		ILista<VOPelicula> catalogo = null;
		if(((ListaEncadenada<VOPelicula>) misPeliculas).darPrimero() != null)
		{
			CompararPeliculas comp = new CompararPeliculas();
			catalogo = mergePeliculas.ordenarPelicula(comp, misPeliculas);
		}
		return catalogo;
	}

	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		ILista<VOGeneroPelicula> listGenPeli = new ListaEncadenada<VOGeneroPelicula>();

		if(genPelicula.darElemento(0)!= null){

			for(int i = 0 ; i < genPelicula.darNumeroElementos(); i++){

				VOGeneroPelicula voGenPel = genPelicula.darElemento(i);
				String genero = voGenPel.getGenero();
				ILista<VOPelicula> pelis = voGenPel.getPeliculas();

				VOPelicula mayor = pelis.darElemento(i);
				double ratingMayor = mayor.getPromedioRatings();
				ILista<VOPelicula> peliculaMayorFinal = new ListaEncadenada<VOPelicula>();

				for(int j = 0 ; j < pelis.darNumeroElementos(); j++){

					double busc = pelis.darElemento(j).getPromedioRatings();
					if( ratingMayor < busc){
						ratingMayor = busc;
						mayor = pelis.darElemento(j);
					}
					peliculaMayorFinal.agregarElementoFinal(mayor);
					voGenPel.setPeliculas(peliculaMayorFinal);
					listGenPeli.agregarElementoFinal(voGenPel);
				}
			}
		}
		return listGenPeli;
	}


	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		
		ILista<VOGeneroPelicula> listFinal = new ListaEncadenada<VOGeneroPelicula>();

		if(genPelicula.darElemento(0)!= null){

			for(int i = 0 ; i < genPelicula.darNumeroElementos()-1; i++){

				VOGeneroPelicula voGenPel = genPelicula.darElemento(i);
				String genero = voGenPel.getGenero();
				ILista<VOPelicula> pelis = (ListaEncadenada<VOPelicula>)voGenPel.getPeliculas();
				
				//Ordenamiento
				ILista<VOPelicula> pelisOrdenadas = null;
				CompararAgnoPelicula comp = new CompararAgnoPelicula();
				pelisOrdenadas = mergePeliculas.ordenarPelicula(comp,pelis);
				
				//Agrego al VOGeneroPelicula
				voGenPel.setPeliculas(pelisOrdenadas);
				listFinal.agregarElementoFinal(voGenPel);
			}
		}
		return listFinal;
	}


	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}


	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		ILista<VORating> ratFinal = new ListaEncadenada<VORating>();
		ILista<VORating> ratSinOrdenar = new ListaEncadenada<VORating>();
		if(ratings != null){
			for( int i = 0 ; i< ratings.darNumeroElementos(); i++){
				if(ratings.darElemento(i).getIdPelicula() == idPelicula){
					VORating califica = ratings.darElemento(i);
					ratSinOrdenar.agregarElementoFinal(califica);
					long usurioConRat = ratings.darElemento(i).getIdUsuario(); 
			
					ILista<VOUsuario> usuarios = new ListaEncadenada<VOUsuario>();
					if(miUser != null){
						for(int j = 0; j< miUser.darNumeroElementos(); j++){
							if(miUser.darElemento(j).getIdUsuario()==usurioConRat){
								usuarios.agregarElementoFinal(miUser.darElemento(j));
							}
						}
					}
					/**Comparator<VOUsuario> comp = new CompararFechaRatings();
					ILista<VOUsuario> ordenada = new ListaEncadenada<VOUsuario>();
					ordenada = mergeUsuarioT.ordenarPorRatingFecha(comp, usuarios);
					ratFinal.agregarElementoFinal(ordenada.darElemento(i));
				*/
				}
			}
			
		}
		
		
		return null;
	}


	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		ILista<VOGeneroUsuario> semiRetorna = new ListaEncadenada<VOGeneroUsuario>();
		ILista<VOGeneroUsuario> retorna = new ListaEncadenada<VOGeneroUsuario>();
		for(int i=0; i<genUser.darNumeroElementos();i++)
		{
			VOGeneroUsuario agrRet = new VOGeneroUsuario();
			ILista<VOUsuarioConteo> usuariosAct = new ListaEncadenada<VOUsuarioConteo>();
			
			for (int x=0;x<genUser.darElementoPosicionActual().getUsuarios().darNumeroElementos();x++)
			{
				int cont =0;
				VOUsuarioConteo agregar = new VOUsuarioConteo();
				long buscado = genUser.darElementoPosicionActual().getUsuarios().darElemento(x).getIdUsuario();
				for (int j=0;j<genUser.darElementoPosicionActual().getUsuarios().darNumeroElementos();j++)
				{
					long compara = genUser.darElementoPosicionActual().getUsuarios().darElementoPosicionActual().getIdUsuario();
					if(compara==buscado)
					{
						cont++;
						genUser.darElementoPosicionActual().getUsuarios().darElementoSiguiente();
					}
				}
				genUser.darElementoPosicionActual().getUsuarios().reiniciarActual();
				agregar.setIdUsuario(buscado);
				agregar.setConteo(cont);
				if(usuariosAct.darNumeroElementos()==0)
				{
				usuariosAct.agregarElementoFinal(agregar);
				}
				else
				{
					boolean encontro = false;
					for(int a=0;a<usuariosAct.darNumeroElementos()&&!encontro;a++)
					{
						if(usuariosAct.darElementoPosicionActual().getIdUsuario()==agregar.getIdUsuario())
						{
							encontro = true;
						}
						usuariosAct.darElementoSiguiente();
					}
					usuariosAct.reiniciarActual();
					if(!encontro)
					{
						usuariosAct.agregarElementoFinal(agregar);
					}
				}
				
			}
			agrRet.setGenero(genUser.darElementoPosicionActual().getGenero());
			agrRet.setUsuarios(usuariosAct);
			semiRetorna.agregarElementoFinal(agrRet);
			genUser.darElementoSiguiente();
		}
		genUser.reiniciarActual();
		ILista<VOUsuarioConteo> ordenada = new ListaEncadenada<VOUsuarioConteo>();
		for (int h=1; h<semiRetorna.darNumeroElementos();h++)
		{
			CompararPorConteo cmp = new CompararPorConteo();
			ordenada = mergeConteo.ordenarPorConteo(cmp, (semiRetorna.darElementoPosicionActual().getUsuarios()));
			semiRetorna.darElementoPosicionActual().setUsuarios(ordenada);
			semiRetorna.darElementoSiguiente();
		}
		semiRetorna.reiniciarActual();
		for(int m=0;m<semiRetorna.darNumeroElementos();m++)
		{
			VOGeneroUsuario agrRetP = new VOGeneroUsuario();
			ILista<VOUsuarioConteo> usuarioA = new ListaEncadenada<VOUsuarioConteo>();
			for(int xh=0; xh<n;xh++)
			{
				usuarioA.agregarElementoFinal(semiRetorna.darElementoPosicionActual().getUsuarios().darElementoPosicionActual());
				semiRetorna.darElementoPosicionActual().getUsuarios().darElementoSiguiente();
			}
			semiRetorna.darElementoPosicionActual().getUsuarios().reiniciarActual();
			agrRetP.setGenero(semiRetorna.darElementoPosicionActual().getGenero());
			agrRetP.setUsuarios(usuarioA);
			retorna.agregarElementoFinal(agrRetP);
			semiRetorna.darElementoSiguiente();
		}
		semiRetorna.reiniciarActual();


		return retorna;
	}


	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() 
	{
		ILista<VOUsuario> a = new ListaEncadenada<>();
		return a; 
	}


	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n)
	{
		ILista<VOGeneroPelicula> retorna = new ListaEncadenada<VOGeneroPelicula>();
		ILista<VOPelicula> ordenada = new ListaEncadenada<>();
		ILista<VOPelicula> ordenar = new ListaEncadenada<VOPelicula>();
		VOGeneroPelicula agregaLista = new VOGeneroPelicula();
		
		for(int i=0; i<genPelicula.darNumeroElementos();i++)
		{
			ordenar = genPelicula.darElementoPosicionActual().getPeliculas();
			//supongamos que orden� la lista ordenar
			
		for(int h=0; h<n;h++)
		{
			ordenada.agregarElementoFinal(ordenar.darElementoPosicionActual());
			ordenar.darElementoSiguiente();
		}
		ordenar.reiniciarActual();
		agregaLista.setGenero(genPelicula.darElementoPosicionActual().getGenero());
		agregaLista.setPeliculas(ordenada);
		retorna.agregarElementoFinal(agregaLista);
		genPelicula.darElementoSiguiente();
		}
		genPelicula.reiniciarActual();
		
		return retorna;
		
	}


	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		ILista<VOUsuarioGenero> retorna = new ListaEncadenada<VOUsuarioGenero>();
		VOUsuarioGenero agregaRet= new VOUsuarioGenero();
		genTag= new ListaEncadenada<VOGeneroTag>();
		long y = Long.parseLong(tagsIdU.darElementoPosicionActual());
		for(int h=0; h<tags.darNumeroElementos();h++)
		{
			if(Long.parseLong(tagsIdU.darElementoPosicionActual())==y)
			{
				long idPelicula =Long.parseLong(tagsIdP.darElementoPosicionActual());
				VOPelicula peliculaB = new VOPelicula();
				while(misPeliculas.darElementoPosicionActual()!=null)
				{
					if(misPeliculas.darElementoPosicionActual().getIdPelicula()==idPelicula)
					{
						peliculaB=misPeliculas.darElementoPosicionActual();
						break;
					}
					misPeliculas.darElementoSiguiente();
				}
				misPeliculas.reiniciarActual();
				for (int i=0; i<peliculaB.getGenerosAsociados().darNumeroElementos();i++) 
				{
					VOGeneroTag generoTag= new VOGeneroTag();
					ILista<String> tagDeGenero = new ListaEncadenada<String>();

					if(genTag.darNumeroElementos()==0)
					{
						tagDeGenero.agregarElementoFinal(tags.darElementoPosicionActual().getTag());
						generoTag.setGenero(peliculaB.getGenerosAsociados().darElementoPosicionActual());
						generoTag.setTags(tagDeGenero);
						genTag.agregarElementoFinal(generoTag);
					}
					else{
						boolean encontro = false;

						for(int j=0;j<genTag.darNumeroElementos()&& !encontro;j++)
						{
							if(peliculaB.getGenerosAsociados().darElementoPosicionActual().equals(genTag.darElementoPosicionActual().getGenero()))
							{

								tagDeGenero= genTag.darElementoPosicionActual().getTags();
								tagDeGenero.agregarElementoFinal(tags.darElementoPosicionActual().getTag());
								genTag.darElementoPosicionActual().setTags(tagDeGenero);
								encontro = true;
							}
							else
								genTag.darElementoSiguiente();
						}
						genTag.reiniciarActual();
						if (encontro != true)
						{	
							tagDeGenero.agregarElementoFinal(tags.darElementoPosicionActual().getTag());
							generoTag.setGenero(peliculaB.getGenerosAsociados().darElementoPosicionActual());
							generoTag.setTags(tagDeGenero);
							genTag.agregarElementoFinal(generoTag);
						}
					}
					peliculaB.getGenerosAsociados().darElementoSiguiente();
				}
				peliculaB.getGenerosAsociados().reiniciarActual();
			}
			else
			{
				agregaRet.setIdUsuario(y);
				agregaRet.setListaGeneroTags(genTag);
				y=Long.parseLong(tagsIdU.darElementoPosicionActual());
				retorna.agregarElementoFinal(agregaRet);
				genTag = new ListaEncadenada<VOGeneroTag>();
				agregaRet = new VOUsuarioGenero();
			}
			tags.darElementoSiguiente();
			tagsIdP.darElementoSiguiente();
			tagsIdU.darElementoSiguiente();
		}
		tagsIdP.reiniciarActual();
		tags.reiniciarActual();
		tagsIdU.reiniciarActual();
		
		return retorna;
	}


	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(
			String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}


	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		return null;
	}


	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		if(operaciones == null)
		{
			operaciones = new Queue<VOOperacion>();
		}
		else
		{
			VOOperacion nueva = new VOOperacion();
			nueva.setOperacion(nomOperacion);
			nueva.setTimestampInicio(tinicio);
			nueva.setTimestampFin(tfin);
			operaciones.enqueue(nueva);
		}

	}


	public ILista<VOOperacion> darHistoralOperacionesSR() {
		Queue<VOOperacion> temp = operaciones;
		Queue<VOOperacion> aux = new Queue<VOOperacion>();
		while(!temp.isEmpty())
		{
			aux.enqueue(temp.dequeue());
		}
		ListaEncadenada<VOOperacion> listaOperaciones = new ListaEncadenada<VOOperacion>();
		for(int i=0; i < temp.size(); i++)
		{
			listaOperaciones.agregarElementoFinal(aux.dequeue());
		}
		return listaOperaciones;
	}


	public void limpiarHistorialOperacionesSR() {
		operaciones = new Queue<VOOperacion>();

	}


	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		Queue<VOOperacion> temp = operaciones;
		Queue<VOOperacion> aux = new Queue<VOOperacion>();
		while(!temp.isEmpty())
		{
			aux.enqueue(temp.dequeue());
		}
		ListaEncadenada<VOOperacion> listaOperaciones = new ListaEncadenada<VOOperacion>();
		for(int i=0; i < n ; i++)
		{
			listaOperaciones.agregarElementoFinal(aux.dequeue());
		}
		return listaOperaciones;
	}


	public void borrarUltimasOperaciones(int n) {
		for(int i=0; i < n; i++)
		{
			operaciones.dequeue();
		}
	}


	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		VORating ratingC = new VORating();
		ratingC.setIdUsuario(idUsuario);
		ratingC.setIdPelicula(idPelicula);
		ratingC.setRating(rating);

		ratings.agregarElementoFinal(ratingC);

	}

}
