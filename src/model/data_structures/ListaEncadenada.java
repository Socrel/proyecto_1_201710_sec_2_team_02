package model.data_structures;



public class ListaEncadenada<T> implements ILista<T> {

	private NodoSimple<T> primero;
	private NodoSimple<T> ultimo;
	private NodoSimple<T> actual;
	private NodoSimple<T> mitad;
	private int tamLista;

	public ListaEncadenada(){
		tamLista = 0;
		primero = null;
		ultimo = primero;
		actual = primero;
		mitad = primero;
	}

	
	public void agregarElementoFinal(T elem) {
		NodoSimple<T> act = new NodoSimple<T>(elem);
		if(primero == null){
			primero = act;
			actual = primero;
			ultimo = primero;
			
		}else{
			ultimo.cambiarSiguiente(act);
			ultimo = act;
		}
		tamLista++;

	}
	
	public void asignarElementoMitad()
	{
		int contador=0;
		NodoSimple<T> actual= primero;
		while(actual!=null)
		{
			if (contador==tamLista/2) {
				mitad = actual;
				break;
			}
			actual=actual.darSiguiente();
			contador++;
		}
	}
	
	public T darElemento(int pos) {
		T resp=null;
		int contador=0;
		NodoSimple<T> actual= primero;
		while(actual!=null)
		{
			T ob= actual.darItem();
			if (contador==pos) {
				resp= ob;
				break;
			}
			actual=actual.darSiguiente();
			contador++;
		}
		return resp;
	}


	
	public int darNumeroElementos() {
		return tamLista;
	}

	
	public T darElementoPosicionActual() {
		return actual.darItem();
	}

	
	public void borrarElementos()
	{
		if(primero.darSiguiente()!=null)
		{
        primero.cambiarSiguiente(null);
        primero = null;
		}
		else 
			primero = null;
		
        tamLista=0;
	}



	@Override
	public T darElementoMitad() {
		// TODO Auto-generated method stub
		return mitad.darItem();
	}


	@Override
	public T darElementoFinal() {
		// TODO Auto-generated method stub
		return ultimo.darItem();
	}


	@Override
	public T darElementoAnterior() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public T darElementoSiguiente() {
		if(actual.darSiguiente()!=null)
		{
		actual= actual.darSiguiente();
		return actual.darItem();
		}
		else 
		{
			actual=null;
			return null;
		}
	}


	@Override
	public void reiniciarActual() {
		actual = primero;
		
	}


	public void cambiarPrimero(NodoSimple<T> elem) {
		primero = elem;
		
	}


	public NodoSimple<T> darPrimero() {
		return primero;
	}


	public NodoSimple<T> darUltimo() {
		return ultimo;
	}


	public void cambiarUltimo(NodoSimple<T> nuevo) {
		ultimo = nuevo;
		
	}
}