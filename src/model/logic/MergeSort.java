package model.logic;
import java.util.Comparator;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoDoble;
import model.data_structures.NodoSimple;
import model.vo.VOPelicula;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;

public class MergeSort<T>
{
	private static ILista auxS = new ListaEncadenada<>();


	//-----------------------------------------------------------------------------------------------------------------------
	//-------------------------------------NodoSimple-------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------------------------------------

	public static <T> void sort(Comparator<T> cmp,NodoSimple<T> head) {

		ListaEncadenada<T> bracket = new ListaEncadenada<T>();

		int k = 1;
		NodoSimple p = head.darSiguiente();
		while (p != null) {
			bracket.cambiarPrimero(p);
			sort(cmp, k, bracket);
			mergeSimple(cmp, head, k, bracket.darPrimero(), k, bracket);
			head =  bracket.darPrimero(); 
			p =  bracket.darUltimo();
			k = 2*k;
		}
	}

	private static <T> void sort(Comparator<T> cmp, int k, ListaEncadenada<T> bracket) 
	{
		if (k < 2) {
			bracket.cambiarUltimo(bracket.darUltimo().darSiguiente());
			return; 
		}
		sort(cmp, k/2, bracket);
		if ( bracket.darUltimo() != null) {
			NodoSimple<T> top = bracket.darPrimero();
			bracket.cambiarPrimero( bracket.darUltimo());
			sort(cmp, k/2, bracket);
			NodoSimple<T> bot =  bracket.darPrimero();
			mergeSimple(cmp, top, k/2, bot, k/2, bracket);
		}
	}

	private static <T> void mergeSimple(Comparator<T> comparator, NodoSimple<T> top, int ctop,NodoSimple<T> bot, int cbot,ILista<T> bracket) {
		int count = ctop+cbot;
		NodoSimple<T> head=null, tail=null, next;
		while (ctop + cbot > 0) {
			if (cbot == 0){
				next = top;
				top = top.darSiguiente();
				ctop--; 
			}
			else if (ctop == 0) {
				next = bot;
				bot = bot.darSiguiente();
				cbot--; 
			}
			else {
				int cmp = comparator.compare(top.darItem(), bot.darItem());
				if (cmp > 0) {
					next = bot; 
					bot = bot.darSiguiente();
					cbot--; 
				}
				else{ 
					next = top; 
					top = top.darSiguiente();
					ctop--; 
				}
			}
			if (head == null) 
				head = next;
			else 
				tail.cambiarSiguiente(next);
			tail = next;
			if (bot == null)
				cbot=0;
		}
		tail.cambiarSiguiente(bot);

		((ListaEncadenada) bracket).cambiarPrimero(head);
		((ListaEncadenada) bracket).cambiarUltimo(tail.darSiguiente());
	}

	public ILista<VOPelicula> ordenarPelicula(Comparator<VOPelicula> comp, ILista<VOPelicula> list){
		sort(comp,((ListaEncadenada) list).darPrimero());
		ILista<VOPelicula> resp = (ListaEncadenada<VOPelicula>)list;
		return resp;
	}

	public ILista<VOUsuarioConteo> ordenarPorConteo(Comparator<VOUsuarioConteo> cmp, ILista<VOUsuarioConteo> list) {
		sort(cmp,((ListaEncadenada) list).darPrimero());
		ILista<VOUsuarioConteo> resp = (ListaEncadenada<VOUsuarioConteo>)list;
		return resp;
	}
	public ILista<VOUsuario> ordenarPorRatingFecha(Comparator<VOUsuario> cmp, ILista<VOUsuario> list) {
		sort(cmp,((ListaEncadenada) list).darPrimero());
		ILista<VOUsuario> resp = (ListaEncadenada<VOUsuario>)list;
		return resp;
	}
}