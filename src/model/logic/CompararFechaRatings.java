package model.logic;

import java.util.Comparator;

import model.vo.VOUsuario;

public class CompararFechaRatings implements Comparator{

	@Override
	public int compare(Object usu1, Object usu2) {
		if( ((VOUsuario) usu1).getPrimerTimestamp() < ((VOUsuario) usu2).getPrimerTimestamp())
			return 1;
		else if(((VOUsuario) usu1).getPrimerTimestamp()> ((VOUsuario) usu2).getPrimerTimestamp())
		return -1;
		else
		return 0;
	}

}
